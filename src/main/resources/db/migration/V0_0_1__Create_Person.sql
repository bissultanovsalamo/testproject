CREATE TABLE test.person
(
    id            serial primary key,
    name          varchar(255),
    surname       varchar(255),
    created_date  TIMESTAMP,
    modified_date TIMESTAMP
);

CREATE TABLE test.contact
(
    id            serial primary key,
    contact       varchar(255),
    type_contact  varchar(255),
    created_date  TIMESTAMP,
    modified_date TIMESTAMP,
    person_id     INTEGER,
    FOREIGN KEY (person_id)
        REFERENCES test.person (id)
        ON DELETE CASCADE
)