package kz.example.test.telbook.service;

import kz.example.test.telbook.dto.PersonDto;
import kz.example.test.telbook.entity.PersonEntity;
import kz.example.test.telbook.repository.PersonRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.beans.Transient;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class PersonService {

    private final PersonRepository personRepository;
    private final ModelMapper modelMapper;

    public PersonDto add(PersonDto personDto) {
        PersonEntity personEntity = personRepository.save(modelMapper.map(personDto, PersonEntity.class));
        return modelMapper.map(personEntity, PersonDto.class);

    }

    public List<PersonDto> getAll() {
        return personRepository.findAll().stream()
                .map(item -> modelMapper.map(item, PersonDto.class)).collect(Collectors.toList());
    }

    public void delete(Integer id) {
        boolean isExist = personRepository.existsById(id);
        if (!isExist) {
            throw new RuntimeException("This person by id not found");
        }
        personRepository.deleteById(id);
        log.info("Delete person by id = " + id + " success.");
    }

    public PersonDto update(PersonDto personDto) {
        boolean isExists = personRepository.existsById(personDto.getId());
        if (!isExists) {
            throw new RuntimeException("This person by id not found");
        }
        PersonEntity person = modelMapper.map(personDto, PersonEntity.class);

        return modelMapper.map(personRepository.save(person), PersonDto.class);
    }

    public PersonDto get(Integer id) {
        Optional<PersonEntity> optional = personRepository.findById(id);
        if (optional.isPresent()) {
            return optional.map(item -> modelMapper.map(item, PersonDto.class)).orElse(null);
        }
        throw new RuntimeException("This person not found for getting by id = " + id);
    }
}
