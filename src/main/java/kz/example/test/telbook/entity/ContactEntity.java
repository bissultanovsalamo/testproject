package kz.example.test.telbook.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name = "contact", schema = "test")
public class ContactEntity {
    @Id
    @Column(name = "id")
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "contact_seq")
    @SequenceGenerator(
            name = "contact_seq",
            sequenceName = "contact_id_seq", schema = " test", allocationSize = 1)
    private Integer id;

    @Column(name = "contact")
    private String contact;

    @Column(name = "type_contact")
    private String typeContact;

    @Column(name = "created_date", updatable = false)
    private LocalDateTime createdDate;

    @Column(name = "modified_date")
    private LocalDateTime modifiedDate;

    @PrePersist
    public void toCreate() {
        setCreatedDate(LocalDateTime.now());
        setModifiedDate(getCreatedDate());
    }

    @PreUpdate
    public void toUpdate() {
        setCreatedDate(getCreatedDate());
        setModifiedDate(LocalDateTime.now());
    }

    @Column(name = "person_id")
    private Integer personId;
}
