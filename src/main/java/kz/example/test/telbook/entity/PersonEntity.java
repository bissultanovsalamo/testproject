package kz.example.test.telbook.entity;


import kz.example.test.telbook.dto.ContactDto;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Data
@Table(name = "person", schema = "test")
public class PersonEntity {
    @Id
    @Column(name = "id")
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "person_seq")
    @SequenceGenerator(
            name = "person_seq",
            sequenceName = "person_id_seq", schema = " test", allocationSize = 1)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "created_date", updatable = false)
    private LocalDateTime createdDate;

    @Column(name = "modified_date")
    private LocalDateTime modifiedDate;

    @PrePersist
    public void toCreate() {
        setCreatedDate(LocalDateTime.now());
        setModifiedDate(getCreatedDate());
    }

    @PreUpdate
    public void toUpdate() {
        setModifiedDate(LocalDateTime.now());
    }

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "person_id")
    private List<ContactEntity> contactDto;
}
