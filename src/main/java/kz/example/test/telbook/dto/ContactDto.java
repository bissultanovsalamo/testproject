package kz.example.test.telbook.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ContactDto {
    private Integer id;
    private String contact;
    private String typeContact;
    private LocalDateTime createdDate;
    private LocalDateTime modifiedDate;
    private Integer personId;
}
