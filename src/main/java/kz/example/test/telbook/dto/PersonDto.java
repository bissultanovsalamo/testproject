package kz.example.test.telbook.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.List;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class PersonDto {
    private Integer id;
    private String name;
    private String surname;
    private LocalDateTime createdDate;
    private LocalDateTime modifiedDate;
    private List<ContactDto> contactDto;
}
