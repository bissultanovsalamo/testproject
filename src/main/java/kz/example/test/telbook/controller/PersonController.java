package kz.example.test.telbook.controller;

import kz.example.test.telbook.dto.PersonDto;
import kz.example.test.telbook.service.PersonService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/person")
@RequiredArgsConstructor
public class PersonController {

    private final PersonService personService;

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public ResponseEntity<PersonDto> add(@RequestBody PersonDto personDto) {
        return ResponseEntity.ok(personService.add(personDto));
    }

    @GetMapping(path = "/all")
    public ResponseEntity<List<PersonDto>> gelAll() {
        return ResponseEntity.ok(personService.getAll());
    }

    @DeleteMapping(path = "/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        personService.delete(id);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<PersonDto> update(@RequestBody PersonDto personDto) {
        return ResponseEntity.ok(personService.update(personDto));
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<PersonDto> get(@PathVariable Integer id) {
        return ResponseEntity.ok(personService.get(id));
    }
}

