FROM openjdk:11

WORKDIR /app

COPY build/libs/task-0.0.1.jar /app/task.jar

EXPOSE 8080

ENTRYPOINT ["java","-jar","/task.jar"]